**[W.I.P]**

# Les Goûters Numériques de La Fab'Brique (Coding goûters)

Dépôt pour les ressources des Goûters Numériques (Coding goûters) organisés par  [La Fab'Brique](http://lafabbrique.org)

Plus d'informations sur [la page du projet sur le Wiki de La Fab'Brique](https://lafabbrique.org/du/wiki/ateliers:les_gouters_numeriques#jeu_collectif).

Licence: [CC-0](https://creativecommons.org/publicdomain/zero/1.0/deed.fr) (Domaine Public)
